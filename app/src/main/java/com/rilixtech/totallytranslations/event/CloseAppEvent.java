package com.rilixtech.totallytranslations.event;

/**
 * Event to be sent to main activity to tell it to kill itself.
 */
public class CloseAppEvent {
}
