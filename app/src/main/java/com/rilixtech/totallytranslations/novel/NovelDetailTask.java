package com.rilixtech.totallytranslations.novel;

import android.os.AsyncTask;
import com.rilixtech.totallytranslations.model.Novel;
import com.rilixtech.totallytranslations.parser.NovelParser;

public class NovelDetailTask extends AsyncTask<Void, Void, Novel> {

  public interface ResultListener {
    void onSuccess(Novel newNovel);
    void onFailure(String error);
  }

  private ResultListener mListener;
  private String mNovelUrl;
  private boolean mWithChapter;

  public NovelDetailTask(String novelUrl, ResultListener listener) {
    mNovelUrl= novelUrl;
    mListener = listener;
    mWithChapter = false;
  }

  public NovelDetailTask(String novelUrl, boolean withChapter, ResultListener listener) {
    mNovelUrl= novelUrl;
    mListener = listener;
    mWithChapter = withChapter;
  }

  @Override protected Novel doInBackground(Void... voids) {
    return NovelParser.getNovelDetails(mNovelUrl, mWithChapter);
  }

  @Override protected void onPostExecute(Novel novel) {
    if (novel == null) {
      mListener.onFailure("Failed to get details");
    } else {
      mListener.onSuccess(novel);
    }
  }
}
