package com.rilixtech.totallytranslations.novel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.model.Chapter;
import java.util.List;

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.ViewHolder> {
  private ItemClickListener mListener;
  private List<Chapter> mChapters;

  public interface ItemClickListener {
    void onItemClicked(Chapter chapter);
  }

  ChapterAdapter(List<Chapter> chapters, ItemClickListener listener) {
    mChapters = chapters;
    mListener = listener;
  }

  @NonNull
  @Override
  public ChapterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    Context context = viewGroup.getContext();
    LayoutInflater inflater = LayoutInflater.from(context);
    View view = inflater.inflate(R.layout.item_chapter, viewGroup, false);
    return new ViewHolder(view);
  }

  @Override public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
    Chapter chapter = mChapters.get(viewHolder.getAdapterPosition());
    viewHolder.tvTitle.setText(chapter.title);
    if (TextUtils.isEmpty(chapter.poster)) {
      viewHolder.tvPoster.setVisibility(View.GONE);
    } else {
      viewHolder.tvPoster.setVisibility(View.VISIBLE);
      viewHolder.tvPoster.setText(chapter.poster);
    }
    if (TextUtils.isEmpty(chapter.releaseDate)) {
      viewHolder.tvReleaseDate.setVisibility(View.INVISIBLE);
    } else {
      viewHolder.tvReleaseDate.setText(chapter.releaseDate);
    }
  }

  @Override public int getItemCount() {
    return mChapters.size();
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_chapter_title_tv) TextView tvTitle;
    @BindView(R.id.item_chapter_release_date_tv) TextView tvReleaseDate;
    @BindView(R.id.item_chapter_poster_tv) TextView tvPoster;

    ViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.item_recent_chapter_cv)
    void onItemClicked() {
      mListener.onItemClicked(mChapters.get(getAdapterPosition()));
    }
  }
}
