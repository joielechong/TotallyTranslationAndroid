package com.rilixtech.totallytranslations.novel;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.rilixtech.totallytranslations.BaseActivity;
import com.rilixtech.totallytranslations.Navigator;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.manager.NovelManager;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Novel;
import java.util.ArrayList;
import java.util.List;

public class NovelDetailsActivity extends BaseActivity {
  public static final String EXTRA_NOVEL_ID = "novelIdExtra";
  public static final String TAG = NovelDetailsActivity.class.getSimpleName();

  public static final int MAX_ITEM_PER_LOAD = 7;

  @BindView(R.id.novel_details_cover_imv) ImageView mImvCover;
  @BindView(R.id.novel_details_description_tv) TextView mTvDescription;
  @BindView(R.id.novel_details_synopsis_tv) TextView mTvSynopsis;
  @BindView(R.id.novel_details_chapters_rv) RecyclerView mRvChapters;
  @BindView(R.id.novel_details_patreon_imb) ImageButton mImbPatreon;
  @BindView(R.id.novel_details_update_novel_pgb) ProgressBar mPgbUpdateNovels;
  @BindView(R.id.novel_details_chapters_update_pgb) ProgressBar mPgbUpdateChapters;
  @BindView(R.id.novel_details_total_chapters_tv) TextView mTvTotalChapters;
  @BindView(R.id.novel_details_nscrlv) NestedScrollView mNsvChapters;
  @BindView(R.id.novel_details_load_chapter_pgb) ProgressBar mPgbLoadMoreChapter;

  private ChapterAdapter mChapterAdapter;
  private Novel mNovel;
  private List<Chapter> mChapters;
  private List<Chapter> mLoadedChapters;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_novel_details);
    ButterKnife.bind(this);
    setBackAsHome();

    Bundle bundle = getIntent().getExtras();
    if (bundle == null) throw new RuntimeException("Please add novel id bundle!!!");
    long id = bundle.getLong(EXTRA_NOVEL_ID);
    loadNovelWith(id);
  }

  private void loadNovelWith(long id) {
    mNovel = NovelManager.getNovelById(id);
    setupNovelWith(mNovel);
  }

  private int mLastLoadedIndex = 0;

  private void setupNovelWith(Novel novel) {
    if (novel == null) throw new RuntimeException("Cant found novel!");
    setActivityTitle(novel.name);
    setupNovelDetails(novel);
    mChapters = novel.chapters;
    mLoadedChapters = new ArrayList<>();
    setupRecyclerView(mLoadedChapters);
    loadChaptersChunk(mChapters);
    setNovelUpdateProgress(novel.chapters.size());
    getNewestNovelDetails(novel);
  }

  private void loadChaptersChunk(List<Chapter> chapters) {
    //mPgbLoadMoreChapter.setVisibility(View.VISIBLE);
    Log.d(TAG, "mLoadedChapter size = " + mLoadedChapters.size());
    Log.d(TAG, "mChapters size = " + mChapters.size());
    Log.d(TAG, "mLastLoadedIndex = " + mLastLoadedIndex);
    int prevLastIndex = mLastLoadedIndex;
    if (chapters.size() > 0) {
      if (chapters.size() > mLastLoadedIndex + MAX_ITEM_PER_LOAD) {
        List<Chapter> list =
            chapters.subList(mLastLoadedIndex, mLastLoadedIndex + MAX_ITEM_PER_LOAD);
        mLoadedChapters.addAll(list);
        mLastLoadedIndex += MAX_ITEM_PER_LOAD;
      } else {
        mLoadedChapters = chapters.subList(mLastLoadedIndex, chapters.size());
        mLastLoadedIndex += chapters.size();
      }
    } else {
      mLoadedChapters = chapters;
    }
    Log.d(TAG, "mLoadedChapter size = " + mLoadedChapters.size());
    Log.d(TAG, "mChapters size = " + mChapters.size());
    Log.d(TAG, "mLastLoadedIndex = " + mLastLoadedIndex);
    mChapterAdapter.notifyItemRangeInserted(prevLastIndex, MAX_ITEM_PER_LOAD);
    //mPgbLoadMoreChapter.setVisibility(View.GONE);
  }

  public static Intent createIntent(Activity activity, long id) {
    Intent intent = new Intent(activity, NovelDetailsActivity.class);
    intent.putExtra(EXTRA_NOVEL_ID, id);
    return intent;
  }

  private void setupNovelDetails(Novel novel) {
    Glide.with(this)
        .load(novel.imageUrl)
        .centerCrop()
        .placeholder(R.drawable.ic_launcher_background)
        .into(mImvCover);

    mTvDescription.setText(novel.description);
    mTvSynopsis.setText(novel.synopsis);
    mTvTotalChapters.setText(getString(R.string.novel_total_chapters, novel.chapters.size()));

    if (TextUtils.isEmpty(novel.patreonUrl)) {
      mImbPatreon.setVisibility(View.GONE);
    } else {
      mImbPatreon.setVisibility(View.VISIBLE);
    }
  }

  @OnClick(R.id.novel_details_patreon_imb)
  protected void onPatreonButtonClicked() {
    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.setData(Uri.parse(mNovel.patreonUrl));
    startActivity(intent);
  }

  private void setupRecyclerView(List<Chapter> chapters) {
    LinearLayoutManager manager = new LinearLayoutManager(this);
    mRvChapters.setLayoutManager(manager);
    mRvChapters.setNestedScrollingEnabled(false);
    mRvChapters.setHasFixedSize(false);
    mChapterAdapter = new ChapterAdapter(chapters,
        chapter -> Navigator.INSTANCE.openChapter(this, chapter.id));
    mRvChapters.setAdapter(mChapterAdapter);
    setNestedScrollForLoadMore();
  }

  private void setNovelUpdateProgress(int novelSize) {
    if (novelSize == 0) {
      mPgbUpdateChapters.setVisibility(View.GONE);
    } else {
      mPgbUpdateChapters.setVisibility(View.VISIBLE);
    }
  }

  private void getNewestNovelDetails(Novel currentNovel) {
    if (currentNovel.chapters.size() == 0) {
      mPgbUpdateNovels.setVisibility(View.VISIBLE);
      mPgbUpdateChapters.setVisibility(View.GONE);
    } else {
      //mChapterAdapter.swap(currentNovel.chapters);
      mPgbUpdateChapters.setVisibility(View.VISIBLE);
    }

    //mNsvChapters.smoothScrollTo(0, 0);

    new NovelDetailTask(currentNovel.url, true, new NovelDetailTask.ResultListener() {
      @Override public void onSuccess(Novel updatedNovel) {
        //syncCurrentNovelChapters(currentNovel, updatedNovel);
        //List<Chapter> newChapters = NovelUtils.getNewChaptersFrom(updatedNovel);
        ////if (newChapters.size() > 0) {
        //currentNovel.chapters.addAll(newChapters);
        //NovelUtils.updateNovel(currentNovel);

        //
        //setupNovelDetails(currentNovel);
        //loadChaptersChunk(mChapters);
        ////}
        //mPgbUpdateNovels.setVisibility(View.GONE);
        //mPgbUpdateChapters.setVisibility(View.GONE);
        updateNovel(currentNovel, updatedNovel);
      }

      @Override public void onFailure(String error) {
        mPgbUpdateNovels.setVisibility(View.GONE);
        mPgbUpdateChapters.setVisibility(View.GONE);
      }
    }).execute();
  }

  private void updateNovel(Novel currentNovel, Novel updatedNovel) {
    mPgbUpdateNovels.setVisibility(View.VISIBLE);
    mPgbUpdateChapters.setVisibility(View.VISIBLE);
    UpdateNovelTask.ResultListener listener = new UpdateNovelTask.ResultListener() {
      @Override public void onNovelSaved(Novel novel) {
        setupNovelDetails(novel);
        loadChaptersChunk(mChapters);
        mPgbUpdateNovels.setVisibility(View.GONE);
        mPgbUpdateChapters.setVisibility(View.GONE);
      }
    };
    new UpdateNovelTask(currentNovel, updatedNovel, listener).execute();
  }

  private void setNestedScrollForLoadMore() {
    mNsvChapters.setOnScrollChangeListener(
        new NestedScrollView.OnScrollChangeListener() {
          @Override
          public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX,
              int oldScrollY) {
            if (v.getChildAt(v.getChildCount() - 1) != null) {
              if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight()
                  - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                loadChaptersChunk(mChapters);
              }
            }
          }
        });
  }
}
