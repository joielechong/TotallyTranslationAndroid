package com.rilixtech.totallytranslations.novel;

import android.os.AsyncTask;
import android.view.View;
import com.rilixtech.totallytranslations.interactor.NovelUtils;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Novel;
import java.util.List;

class UpdateNovelTask extends AsyncTask<Void, Void, Novel> {
  private Novel currentNovel;
  private Novel updatedNovel;

  private ResultListener mListener;

  public interface ResultListener {
    void onNovelSaved(Novel novel);
  }

  public UpdateNovelTask(Novel currentNovel,
      Novel updatedNovel, ResultListener listener) {
    this.currentNovel = currentNovel;
    this.updatedNovel = updatedNovel;
    this.mListener = listener;
  }

  @Override protected Novel doInBackground(Void... voids) {
    List<Chapter> newChapters = NovelUtils.getNewChaptersFrom(updatedNovel);
    //if (newChapters.size() > 0) {
    currentNovel.chapters.addAll(newChapters);
    NovelUtils.updateNovel(currentNovel);

    return currentNovel;
  }

  @Override protected void onPostExecute(Novel novel) {
    mListener.onNovelSaved(novel);
  }
}
