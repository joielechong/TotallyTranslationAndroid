package com.rilixtech.totallytranslations;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.BindView;

public abstract class BaseActivity extends AppCompatActivity {

  @BindView(R.id.toolbar) Toolbar toolbar;

  public void setActivityTitle(String title) {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) actionBar.setTitle(title);
  }

  protected void setBackAsHome() {
    setSupportActionBar(toolbar);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      // Respond to the action bar's Up/Home button
      case android.R.id.home:
        finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }
}
