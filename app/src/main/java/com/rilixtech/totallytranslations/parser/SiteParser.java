package com.rilixtech.totallytranslations.parser;

import android.util.Pair;
import com.rilixtech.totallytranslations.EndPoint;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Novel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SiteParser {

  /**
   * Get list of novels with its brief and get recent chapters from site.
   * @return list of novels and list of recent chapters
   */
  public static Pair<List<Novel>, List<Chapter>> getSiteNovels() {
    Connection.Response response;
    try {
      response = Jsoup.connect(EndPoint.SERVER)
          .ignoreContentType(true)
          .userAgent(EndPoint.AGENT)
          .referrer(EndPoint.REFERRER)
          .timeout(EndPoint.TIME_OUT)
          .followRedirects(true)
          .execute();

      Document doc = response.parse();
      Element menu = doc.select("nav.fusion-main-menu").first();
      Element menuList = menu.select("ul.fusion-menu").first();
      Elements elements = menuList.getElementsByClass("menu-item");

      List<Novel> novels = getNovels(elements);
      List<Chapter> chapters = getRecentChapters(doc);
      return new Pair<>(novels, chapters);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Get brief novels from site
   * @param elements html element of novel in site.
   * @return list of novels.
   */
  private static List<Novel> getNovels(Elements elements) {
    List<Novel> novels = new ArrayList<>();
    for (Element el : elements) {
      // exclude non-novel from menu
      if (el.hasClass("menu-item-type-post_type")) continue;

      Novel novel = new Novel();
      novel.name = el.text();
      Element link = el.select("a").first();
      novel.url = link.attr("href");
      novels.add(novel);
    }
    return novels;
  }

  private static List<Chapter> getRecentChapters(Document doc) {
    Element recentChapter = doc.select("div.recent-chapters").first();
    Element list = recentChapter.select("ul.home-chapters-list").first();
    Elements elements = list.getElementsByTag("li");

    List<Chapter> chapters = new ArrayList<>();
    for (Element el : elements) {
      Element chapterTitle = el.select("span.chapter-title").first();
      Element linkChapterTitle = chapterTitle.select("a").first();
      String linkHref = linkChapterTitle.attr("href");

      Element novelTitle = el.select("span.novel-title").first();
      Element linkNovel = novelTitle.select("a").first();
      String linkNovelHref = linkNovel.attr("href");

      Element poster = el.select("span.author-name").first();
      Element releaseDate = el.select("span.release-date").first();

      Chapter chapter = new Chapter();
      chapter.title = linkChapterTitle.text();
      chapter.recentTitle = chapter.title;
      chapter.url = linkHref;
      chapter.novelTitle = linkNovel.text();
      chapter.novelUrl = linkNovelHref;
      chapter.poster = poster.text();
      chapter.releaseDate = releaseDate.text();

      chapters.add(chapter);
    }
    return chapters;
  }
}
