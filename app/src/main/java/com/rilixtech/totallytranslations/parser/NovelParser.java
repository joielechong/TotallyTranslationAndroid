package com.rilixtech.totallytranslations.parser;

import com.rilixtech.totallytranslations.EndPoint;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Novel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class NovelParser {
  public static Novel getNovelDetails(String novelUrl, boolean withChapter) {
    try {
      Connection.Response response = Jsoup.connect(novelUrl)
          .ignoreContentType(true)
          .maxBodySize(2000 * 1024) // 2 MB size.
          .userAgent(EndPoint.AGENT)
          .referrer(EndPoint.REFERRER)
          .timeout(EndPoint.TIME_OUT)
          .followRedirects(true)
          .execute();

      if (response.statusCode() != 200) return null;

      response.charset("UTF-8");
      Document doc = response.parse();

      Element elTitle = doc.select("h2.entry-title").first();
      Element elAuthor = doc.select("p.novel-author").first();
      Element elNovelImage = doc.select("div.novel-image").first();
      Element elImage = elNovelImage.select("img").first();
      String src = elImage.attr("src");
      Element elDescription = doc.select("div.novel-description").first();
      Element elSynopsis = doc.select("div.novel-synopis").first();

      String patreonUrl = "";
      Element elPatreon = doc.select("a.patreon-btn").first();
      if (elPatreon != null) {
        Element elHref = elPatreon.select("a").first();
        patreonUrl = elHref.attr("href");
      }

      List<Chapter> chapters;
      if (withChapter) {
        Element elChapterList = doc.select("div.chapters-list").first();
        Elements elChapters = elChapterList.getElementsByTag("li");
        chapters = getChaptersFrom(elChapters);
      } else {
        chapters = Collections.emptyList();
      }

      Novel novel = new Novel();
      novel.name = elTitle.text();
      novel.author = elAuthor.text();
      novel.imageUrl = src;
      novel.description = elDescription.text();
      novel.synopsis = elSynopsis.text();
      novel.patreonUrl = patreonUrl;
      novel.chapters.addAll(chapters);

      return novel;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  private static List<Chapter> getChaptersFrom(Elements chaptersEl) {
    List<Chapter> chapters = new ArrayList<>();
    for (Element el : chaptersEl) {
      Element linkChapterTitle = el.select("a[href]").first();
      String linkHref = linkChapterTitle.attr("href");
      Chapter chapter = new Chapter();
      chapter.url = linkHref;
      chapter.title = el.text();
      chapters.add(chapter);
    }
    return chapters;
  }
}
