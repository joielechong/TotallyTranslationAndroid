package com.rilixtech.totallytranslations.parser;

import android.text.TextUtils;
import android.util.Pair;
import com.rilixtech.totallytranslations.EndPoint;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Navigation;
import java.io.IOException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ChapterParser {
  public static Pair<Chapter, Navigation> getChapterDetails(Chapter chapter) {
    try {
      Connection.Response response = Jsoup.connect(chapter.url)
          .ignoreContentType(true)
          .userAgent(EndPoint.AGENT)
          .referrer(EndPoint.REFERRER)
          .timeout(EndPoint.TIME_OUT)
          .followRedirects(true)
          .execute();

      Document doc = response.parse();
      Element content = doc.select("div.post-content").first();

      // get the chapter title
      Element titleEl = doc.select("h1.entry-title").first();
      String title = titleEl.text();
      // use first string of content as the title if not found with previous element.
      if(TextUtils.isEmpty(title)) {
        titleEl = content.selectFirst("p");
        title = titleEl.text();
      }
      chapter.title = title.trim();

      // Remove share button
      doc.select("button.simplefavorite-button").remove();

      Element navElement = doc.select("div.single-navigation").first();
      // get navigation previous and next
      Element previous = navElement.select("[rel=\"prev\"]").first();
      Element next = navElement.select("[rel=\"next\"]").first();

      Navigation navigation = new Navigation();
      navigation.setChapter(chapter);
      if(previous != null) {
        Element linkPrevious = previous.select("a").first();
        String linkPreviousHref = linkPrevious.attr("href");
        navigation.setPreviousUrl(linkPreviousHref);
      }
      if(next != null) {
        Element linkNext = next.select("a").first();
        String linkNextHref = linkNext.attr("href");
        navigation.setNextUrl(linkNextHref);
      }

      // now remove navigation from the content
      if(previous != null) previous.remove();
      if(next != null) next.remove();

      chapter.content =content.html();
      return new Pair<>(chapter, navigation);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }
}
