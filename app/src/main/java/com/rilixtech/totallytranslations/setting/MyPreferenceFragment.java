package com.rilixtech.totallytranslations.setting;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.util.Log;
import com.pixplicity.easyprefs.library.Prefs;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.event.CloseAppEvent;
import com.rilixtech.totallytranslations.event.UpdateAllEvent;
import com.rilixtech.totallytranslations.interactor.DbUtils;
import com.rilixtech.totallytranslations.update.UpdateDailyJob;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;
import com.takisoft.fix.support.v7.preference.TimePickerPreference;
import org.greenrobot.eventbus.EventBus;

public class MyPreferenceFragment extends PreferenceFragmentCompat {

  public static final String TAG = "MyPreferenceFragment";

  private CheckBoxPreference mCbxPrefUpdateOnStartUp;
  private TimePickerPreference mTpPrefUpdateTime;

  @Override
  public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
    setPreferencesFromResource(R.xml.my_preferences, rootKey);
    Context context = getActivity();
    setPreferenceUpdateOnStartUp();
    setPreferenceReset(context);
    setPreferenceUpdateAll(context);
    mTpPrefUpdateTime = setPreferenceAutoUpdateTime();
    setPreferenceAutoUpdate(mTpPrefUpdateTime);
  }

  private void setPreferenceUpdateOnStartUp() {
    mCbxPrefUpdateOnStartUp =
        (CheckBoxPreference) findPreference(getString(R.string.pref_key_update_when_start_up));
  }

  private void setPreferenceReset(Context context) {
    Preference prefResetDatabase = findPreference(getString(R.string.pref_key_reset_database));
    prefResetDatabase.setOnPreferenceClickListener(preference -> {
      new AlertDialog.Builder(context)
          .setTitle(R.string.preference_reset_database_title)
          .setMessage(R.string.preference_reset_database_message)
          .setPositiveButton(R.string.preference_ok_button,
              (dialog, which) -> {
                resetPreferences();
                showMessageAppIsClosed();
              })
          .setNegativeButton(R.string.preference_cancel_button,
              (dialog, which) -> {
              })
          .setCancelable(true)
          .show();

      return true;
    });
  }

  private void setPreferenceUpdateAll(Context context) {
    Preference updateAllPref = findPreference(getString(R.string.pref_key_update_all));
    updateAllPref.setOnPreferenceClickListener(preference -> {
      new AlertDialog.Builder(context)
          .setTitle(R.string.preference_update_all_title)
          .setMessage(R.string.preference_update_all_message)
          .setPositiveButton(R.string.preference_ok_button,
              (dialog, which) -> EventBus.getDefault().post(new UpdateAllEvent()))
          .setNegativeButton(R.string.preference_cancel_button,
              (dialog, which) -> {
              })
          .setCancelable(true)
          .show();

      return true;
    });
  }

  private TimePickerPreference setPreferenceAutoUpdateTime() {
    TimePickerPreference pref =
        (TimePickerPreference) findPreference(getString(R.string.pref_key_auto_update_time));

    pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
      @Override public boolean onPreferenceChange(Preference preference, Object o) {
        setUpdateSchedule(true);
        return true;
      }
    });
    return pref;
  }

  private void setPreferenceAutoUpdate(TimePickerPreference prefAutoUpdateTime) {
    CheckBoxPreference pref =
        (CheckBoxPreference) findPreference(getString(R.string.pref_key_auto_update));
    boolean isChecked = pref.isChecked();
    prefAutoUpdateTime.setEnabled(isChecked);

    pref.setOnPreferenceChangeListener((preference, newValue) -> {
      if (newValue instanceof Boolean) {
        boolean isAutoUpdateEnabled = (boolean) newValue;
        prefAutoUpdateTime.setEnabled(isAutoUpdateEnabled);
        setUpdateSchedule(isAutoUpdateEnabled);
      }
      return true;
    });
  }

  private void setUpdateSchedule(boolean isSchedule) {
    if(isSchedule) {
      int hour = mTpPrefUpdateTime.getHourOfDay() + 1;
      int minute = mTpPrefUpdateTime.getMinute();
      if(hour == 0 && minute == 0) return;

      Log.d(TAG, "hour = " + hour);
      Log.d(TAG, "minute = " + minute);
      UpdateDailyJob.scheduleBetween(hour, minute);
      //UpdateDailyJob.runJobImmediately();
    } else {
      UpdateDailyJob.cancelPreviousJob();
    }
  }
  private void showMessageAppIsClosed() {
    new AlertDialog.Builder(getActivity())
        .setTitle(R.string.preference_close_app_title)
        .setMessage(R.string.preference_close_app_message)
        .setPositiveButton(R.string.preference_ok_button,
            (dialog, which) -> EventBus.getDefault().post(new CloseAppEvent()))
        .setCancelable(false)
        .show();
  }

  private void resetPreferences() {
    DbUtils.resetDatabase();
    Prefs.remove(getString(R.string.pref_key_is_first_usage));
    mCbxPrefUpdateOnStartUp.setChecked(false);
  }
}