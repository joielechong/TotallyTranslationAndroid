package com.rilixtech.totallytranslations.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;
import com.rilixtech.totallytranslations.R;

public class PrefUtil {
  private static SharedPreferences mPrefs;
  public static boolean isUpdateChapterWhenOpening(Context ctx) {
    if(mPrefs == null) mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
    return mPrefs.getBoolean(ctx.getString(R.string.pref_key_update_chapter_when_opening), false);
  }
}
