package com.rilixtech.totallytranslations.setting;

import android.os.Bundle;
import butterknife.ButterKnife;
import com.rilixtech.totallytranslations.BaseActivity;
import com.rilixtech.totallytranslations.R;

public class SettingActivity extends BaseActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_setting);
    ButterKnife.bind(this);
    setBackAsHome();

    getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.setting_place_holder_fly, new MyPreferenceFragment())
                .commit();
  }
}
