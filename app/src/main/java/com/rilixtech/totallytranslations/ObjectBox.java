package com.rilixtech.totallytranslations;

import android.content.Context;
import android.util.Log;
import com.rilixtech.totallytranslations.model.MyObjectBox;
import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;

public class ObjectBox {
  private static BoxStore boxStore;

  static void init(Context context) {
    boxStore = MyObjectBox.builder()
        .androidContext(context.getApplicationContext())
        .build();
    if (BuildConfig.DEBUG) {
      boolean started = new AndroidObjectBrowser(boxStore).start(context);
      Log.i("ObjectBrowser", "Started: " + started);
    }
  }

  public static BoxStore get() {
    return boxStore;
  }
}
