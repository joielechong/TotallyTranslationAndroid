package com.rilixtech.totallytranslations.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.freegeek.android.materialbanner.holder.Holder;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.model.Novel;

public class NovelHolderView implements Holder<Novel> {
  private ImageView imageView;
  private TextView title;

  @Override
  public View createView(Context context) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_novel, null);
    imageView = view.findViewById(R.id.item_novel_list_image_imv);
    title = view.findViewById(R.id.item_novel_list_title_tv);
    return view;
  }

  @Override
  public void UpdateUI(Context context, int position, Novel novel) {
    title.setText(novel.name);
    Glide
        .with(context)
        .load(novel.imageUrl)
        .centerCrop()
        .placeholder(R.drawable.ic_launcher_background)
        .into(imageView);
  }
}