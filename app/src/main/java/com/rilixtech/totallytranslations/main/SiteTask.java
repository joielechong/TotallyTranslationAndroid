package com.rilixtech.totallytranslations.main;

import android.os.AsyncTask;
import android.util.Pair;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Novel;
import com.rilixtech.totallytranslations.parser.SiteParser;
import java.util.List;

public class SiteTask extends AsyncTask<Void, Void, Pair<List<Novel>, List<Chapter>>> {

  public interface ResultListener {
    void onSuccess(Pair<List<Novel>, List<Chapter>> novelsAndRecents);
    void onFailure(String error);
  }

  private ResultListener listener;

  public SiteTask(ResultListener listener) {
    this.listener = listener;
  }

  @Override protected Pair<List<Novel>, List<Chapter>> doInBackground(Void... voids) {
    return SiteParser.getSiteNovels();
  }

  @Override protected void onPostExecute(Pair<List<Novel>, List<Chapter>> novelAndRecents) {
    if (novelAndRecents == null) {
      listener.onFailure("Failed to download novel list");
    } else {
      listener.onSuccess(novelAndRecents);
    }
  }
}
