package com.rilixtech.totallytranslations.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.freegeek.android.materialbanner.MaterialBanner;
import com.freegeek.android.materialbanner.view.indicator.CirclePageIndicator;
import com.pixplicity.easyprefs.library.Prefs;
import com.rilixtech.totallytranslations.AboutActivity;
import com.rilixtech.totallytranslations.Navigator;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.Utils;
import com.rilixtech.totallytranslations.event.CloseAppEvent;
import com.rilixtech.totallytranslations.event.UpdateAllEvent;
import com.rilixtech.totallytranslations.interactor.ChapterUtils;
import com.rilixtech.totallytranslations.interactor.NovelUtils;
import com.rilixtech.totallytranslations.manager.HistoryManager;
import com.rilixtech.totallytranslations.manager.NovelManager;
import com.rilixtech.totallytranslations.manager.SiteManager;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Novel;
import com.rilixtech.totallytranslations.model.RecentChapter;
import com.rilixtech.totallytranslations.novel.NovelDetailTask;
import com.rilixtech.totallytranslations.setting.SettingActivity;
import java.util.ArrayList;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {
  @BindView(R.id.main_novel_loading_tv) TextView mTvNovelLoading;
  @BindView(R.id.main_novel_loading_pgb) ProgressBar mPgbNovelLoading;
  @BindView(R.id.main_recent_chapters_rv) RecyclerView mRvRecentChapters;
  @BindView(R.id.main_recent_chapters_tv) TextView mTvRecentChaptersTitle;
  @BindView(R.id.main_recent_chapters_loading_tv) TextView mTvRecentChaptersLoading;
  @BindView(R.id.main_recent_chapters_loading_pgb) ProgressBar mPgbRecentChaptersLoading;
  @BindView(R.id.main_recent_chapters_update_loading_pgb) ProgressBar mPgbRecentChaptersUpdateloading;
  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.drawer_layout) DrawerLayout drawer;
  @BindView(R.id.nav_view) NavigationView navigationView;
  @BindView(R.id.main_novel_mbr) MaterialBanner mMbrNovel;

  private RecentChaptersAdapter mRecentChaptersAdapter;
  private List<Novel> mNovels;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    setTitle(R.string.main_title);
    setupDrawer();
    initRecentChaptersRecyclerView();
    EventBus.getDefault().register(this);
    loadSiteDetails();
  }

  private void loadSiteDetails() {
    String startUpKey = getString(R.string.pref_key_update_when_start_up);
    boolean isUpdateOnStartUp;

    // check if app is first time usage
    String isFirstTimeUsageKey = getString(R.string.pref_key_is_first_usage);
    boolean isFirstTimeUsage = Prefs.getBoolean(isFirstTimeUsageKey, true);
    // get the details if app is first usage.
    if (isFirstTimeUsage) {
      isUpdateOnStartUp = true;
      Prefs.putBoolean(isFirstTimeUsageKey, false);
    } else {
      isUpdateOnStartUp = Prefs.getBoolean(startUpKey, false);
    }
    getSiteDetails(isUpdateOnStartUp);
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onMessageEvent(CloseAppEvent event) {
    finish();
    Utils.closeApp(this);
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onMessageEvent(UpdateAllEvent event) {
    getSiteDetails(true);
  }

  @Override protected void onDestroy() {
    EventBus.getDefault().unregister(this);
    super.onDestroy();
  }

  private void showNovelLoading(boolean show) {
    mTvNovelLoading.setVisibility(show ? View.VISIBLE : View.GONE);
    mPgbNovelLoading.setVisibility(show ? View.VISIBLE : View.GONE);
  }

  private void showRecentChaptersLoading(boolean show) {
    mTvRecentChaptersLoading.setVisibility(show ? View.VISIBLE : View.GONE);
    mPgbRecentChaptersLoading.setVisibility(show ? View.VISIBLE : View.GONE);
    mTvRecentChaptersTitle.setVisibility(show ? View.GONE : View.VISIBLE);
  }

  private void showRecentChaptersUpdating(boolean show) {
    mPgbRecentChaptersUpdateloading.setVisibility(show ? View.VISIBLE : View.GONE);
  }

  private void setupDrawer() {
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();
    navigationView.setNavigationItemSelectedListener(this);
  }

  @Override
  public void onBackPressed() {
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    //if (id == R.id.nav_camera) {
    //  // Handle the camera action
    //} else if (id == R.id.nav_gallery) {
    //
    //} else if (id == R.id.nav_slideshow) {
    //
    //} else

    if (id == R.id.nav_settings) {
      startActivity(new Intent(this, SettingActivity.class));
    } else if (id == R.id.nav_share) {
      // TODO: Share app
    } else if (id == R.id.nav_developer) {
      startActivity(new Intent(this, AboutActivity.class));
    } else if(id == R.id.nav_last_read) {
      Navigator.INSTANCE.openLastRead(this);
    }
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  private void getNovelFromServer(List<Novel> novels) {
    showNovelLoading(true);
    for (int i = 0; i < novels.size(); i++) {
      Novel novel = novels.get(i);
      new NovelDetailTask(novel.url, new NovelDetailTask.ResultListener() {
        @Override public void onSuccess(Novel newNovel) {
          novel.id = NovelManager.updateNovel(novel, newNovel).id;
          tempNovelDownloadCounter++;
          mNovels = novels;
          if (tempNovelDownloadCounter >= novels.size()) {
            showNovelLoading(false);
            setupNovelBanners(mNovels);
          }
        }

        @Override public void onFailure(String error) {
          setupNovelBanners(mNovels);
          showNovelLoading(false);
        }
      }).execute();
    }
  }

  private void getSiteDetails(boolean isUpdateNovelFromServer) {
    showNovelLoading(true);
    showRecentChaptersLoading(true);

    // load previous novels.
    mNovels = NovelUtils.getAllNovels();
    setupNovelBanners(mNovels);

    List<RecentChapter> chapters = ChapterUtils.getRecentChapters();
    if (chapters.size() > 0) {
      showRecentChaptersLoading(false);
      showRecentChaptersUpdating(true);
      updateRecentChaptersRecyclerView(chapters);
    }

    // get novels and recent update from server.
    getSiteDetailsFromServer(isUpdateNovelFromServer);
  }

  /**
   * Get all novels from local database
   */
  private int tempNovelDownloadCounter = 0;

  private void getSiteDetailsFromServer(boolean isUpdateNovelFromServer) {
    new SiteTask(new SiteTask.ResultListener() {
      @Override
      public void onSuccess(final Pair<List<Novel>, List<Chapter>> novelsAndRecents) {
        if (isUpdateNovelFromServer) {
          getNovelFromServer(novelsAndRecents.first);
        } else {
          showNovelLoading(false);
        }

        SiteManager.createOrUpdateNovels(novelsAndRecents.first);
        List<RecentChapter> updatedChapters =
            SiteManager.saveAndGetRecentChaptersToDB(MainActivity.this, novelsAndRecents.second);

        showRecentChaptersLoading(false);
        showRecentChaptersUpdating(false);
        updateRecentChaptersRecyclerView(updatedChapters);

        //TODO: This is not working!!
        List<Chapter> chapters = HistoryManager.getLastUnreadChapters();
        //
        //Toast.makeText(MainActivity.this, "There is " + chapters.size() + " new chapters",
        //    Toast.LENGTH_LONG).show();
      }

      @Override public void onFailure(String error) {
        showRecentChaptersLoading(false);
        showRecentChaptersUpdating(false);
        showNovelLoading(false);
      }
    }).execute();
  }

  private void setupNovelBanners(List<Novel> novels) {
    if(novels.size() == 0) return;

    mMbrNovel.setPages(NovelHolderView::new, novels);
    mMbrNovel.startTurning(3000);
    mMbrNovel.setIndicator(new CirclePageIndicator(this));
    mMbrNovel.setOnItemClickListener(
        position -> Navigator.INSTANCE.openNovel(MainActivity.this, novels.get(position)));
  }

  private void updateRecentChaptersRecyclerView(List<RecentChapter> chapters) {
    mRecentChaptersAdapter.swap(chapters);
    mTvRecentChaptersTitle.setText(getString(R.string.recent_chapters_with_total, chapters.size()));
  }

  private void initRecentChaptersRecyclerView() {
    List<RecentChapter> chapters = new ArrayList<>();
    mRecentChaptersAdapter = new RecentChaptersAdapter(chapters,
        recentChapter -> Navigator.INSTANCE.openChapter(MainActivity.this,
            recentChapter.chapter.getTargetId()));
    LinearLayoutManager manager = new LinearLayoutManager(this);
    mRvRecentChapters.setAdapter(mRecentChaptersAdapter);
    mRvRecentChapters.setLayoutManager(manager);
    mTvRecentChaptersTitle.setText(getString(R.string.recent_chapters_with_total, chapters.size()));
  }
}
