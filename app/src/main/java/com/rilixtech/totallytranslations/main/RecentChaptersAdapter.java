package com.rilixtech.totallytranslations.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.RecentChapter;
import java.util.List;

public class RecentChaptersAdapter extends RecyclerView.Adapter<RecentChaptersAdapter.ViewHolder> {

  private List<RecentChapter> mRecentChapters;

  public interface ItemClickListener {
    void onItemClicked(RecentChapter recentChapter);
  }

  private ItemClickListener mListener;

  RecentChaptersAdapter(List<RecentChapter> recentChapters, ItemClickListener listener) {
    mRecentChapters = recentChapters;
    mListener = listener;
  }

  @NonNull
  @Override
  public RecentChaptersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    Context context = viewGroup.getContext();
    LayoutInflater inflater = LayoutInflater.from(context);
    View view = inflater.inflate(R.layout.item_recent_chapter, viewGroup, false);
    return new ViewHolder(view);
  }

  @Override public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
    RecentChapter recentChapter = mRecentChapters.get(viewHolder.getAdapterPosition());

    Chapter chapter = recentChapter.chapter.getTarget();
    viewHolder.tvTitle.setText(chapter.title);
    viewHolder.tvNovelName.setVisibility(View.VISIBLE);
    viewHolder.tvNovelName.setText(chapter.novelTitle);
    if(!TextUtils.isEmpty(chapter.poster)) {
      viewHolder.tvPoster.setText(chapter.poster);
      viewHolder.tvPoster.setVisibility(View.VISIBLE);
    }
    viewHolder.tvReleaseDate.setText(chapter.releaseDate);
  }

  @Override public int getItemCount() {
    return mRecentChapters.size();
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_chapter_title_tv) TextView tvTitle;
    @BindView(R.id.item_chapter_release_date_tv) TextView tvReleaseDate;
    @BindView(R.id.item_chapter_novel_name_tv) TextView tvNovelName;
    @BindView(R.id.item_chapter_poster_tv) TextView tvPoster;

    ViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.item_recent_chapter_cv)
    void onItemClicked() {
      mListener.onItemClicked(mRecentChapters.get(getAdapterPosition()));
    }
  }

  public void swap(@NonNull List<RecentChapter> chapters) {
    mRecentChapters.clear();
    mRecentChapters.addAll(chapters);
    notifyDataSetChanged();
  }
}
