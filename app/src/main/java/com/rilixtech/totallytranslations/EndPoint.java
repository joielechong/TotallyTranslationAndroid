package com.rilixtech.totallytranslations;

public class EndPoint {
  public static final String SERVER = "https://totallytranslations.com/";
  public static final String AGENT = "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
  public static final String REFERRER = "http://www.google.com";
  public static final int TIME_OUT = 12000; // in millis
}
