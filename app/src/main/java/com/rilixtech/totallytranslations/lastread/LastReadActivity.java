package com.rilixtech.totallytranslations.lastread;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.rilixtech.totallytranslations.BaseActivity;
import com.rilixtech.totallytranslations.Navigator;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.interactor.ChapterUtils;
import com.rilixtech.totallytranslations.model.LastReadChapter;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LastReadActivity extends BaseActivity {

  @BindView(R.id.last_read_rv) RecyclerView mRvLastRead;
  LastReadChapterAdapter mAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_last_read);
    ButterKnife.bind(this);
    setBackAsHome();
    setupRecyclerView(mRvLastRead, mAdapter);
    EventBus.getDefault().register(this);
  }

  @Override protected void onDestroy() {
    EventBus.getDefault().unregister(this);
    super.onDestroy();
  }

  private void setupRecyclerView(RecyclerView recyclerView, LastReadChapterAdapter adapter) {
    List<LastReadChapter> chapters = ChapterUtils.getLastReadChapters();
    LastReadChapterAdapter.ItemClickListener listener =
        chapter -> Navigator.INSTANCE.openChapter(this, chapter.chapter.getTargetId());
    adapter = new LastReadChapterAdapter(chapters, listener);

    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(adapter);
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onMessageEvent(RefreshLastReadEvent event) {
    List<LastReadChapter> chapters = ChapterUtils.getLastReadChapters();
    mAdapter.swap(chapters);
  }
}
