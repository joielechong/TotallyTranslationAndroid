package com.rilixtech.totallytranslations.update;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.evernote.android.job.DailyJob;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.pixplicity.easyprefs.library.Prefs;
import com.rilixtech.totallytranslations.App;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.main.MainActivity;
import com.rilixtech.totallytranslations.manager.SiteManager;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public final class UpdateDailyJob extends DailyJob {

  public static final String TAG = "UpdateDailyJob";
  private static final String PREF_JOB_ID = "jobIdPref";

  public static void scheduleBetween(int startHour, int startMinute) {
    cancelPreviousJob();

    // schedule between start and end hour
    long startMillis = TimeUnit.HOURS.toMillis(startHour) + TimeUnit.MINUTES.toMillis(startMinute);
    long endMillis = startMillis + TimeUnit.MINUTES.toMillis(30);
    JobRequest.Builder builder = new JobRequest
        .Builder(TAG)
        .setExecutionWindow(startMillis, endMillis)
        .setRequiredNetworkType(JobRequest.NetworkType.UNMETERED)
        .setRequirementsEnforced(true);

    int jobId = DailyJob.schedule(builder, startMillis, endMillis);

    Prefs.putInt(PREF_JOB_ID, jobId);
  }

  @NonNull
  @Override
  protected DailyJobResult onRunDailyJob(Params params) {
    Log.d(TAG, "onRunDailyJob called");
    App app = App.getInstance();
    Context context = app.getApplicationContext();
    showNotification(context, context.getString(R.string.daily_update_starting_content));
    SiteManager.getSiteDetailsSynchronous(context);
    showNotification(context, context.getString(R.string.daily_update_ending_content));
    return DailyJobResult.SUCCESS;
  }

  public static void cancelPreviousJob() {
    int prevJobId = Prefs.getInt(PREF_JOB_ID, -1);
    if (prevJobId != -1) JobManager.instance().cancel(prevJobId);
  }

  private static void showNotification(Context context, String content) {
    Intent intent = new Intent(context, MainActivity.class);
    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

    String id = "dailyUpdateJobChannel";
    String name = "dailyUpdateJob";

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationChannel channel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_LOW);

      channel.setDescription(context.getString(R.string.daily_update_description));
      NotificationManager manager = context.getSystemService(NotificationManager.class);
      manager.createNotificationChannel(channel);
    }

    Notification notification =
        new NotificationCompat.Builder(context, id)
            .setContentTitle(context.getString(R.string.daily_update_title))
            .setContentText(content)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setShowWhen(true)
            .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            .setLocalOnly(true)
            .build();

    NotificationManagerCompat.from(context).notify(new Random().nextInt(), notification);
  }
}
