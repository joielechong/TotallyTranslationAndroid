package com.rilixtech.totallytranslations.update;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class UpdateJobCreator implements JobCreator {

  @Override
  @Nullable
  public Job create(@NonNull String tag) {
    switch (tag) {
      //case DemoSyncJob.TAG:
      //    return new DemoSyncJob();
      case UpdateDailyJob.TAG:
        return new UpdateDailyJob();
      default:
        return null;
    }
  }
}
