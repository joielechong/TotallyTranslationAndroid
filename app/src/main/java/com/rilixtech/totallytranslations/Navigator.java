package com.rilixtech.totallytranslations;

import android.app.Activity;
import android.content.Intent;
import com.rilixtech.totallytranslations.chapter.ChapterDetailsActivity;
import com.rilixtech.totallytranslations.lastread.LastReadActivity;
import com.rilixtech.totallytranslations.model.Novel;
import com.rilixtech.totallytranslations.novel.NovelDetailsActivity;

public enum Navigator {
  INSTANCE("id", 1);

  // Attributes
  private String str;
  private int i;

  // Constructor
  Navigator(String str, int i){
    this.str = str;
    this.i = i;
  }

  public void openChapter(Activity activity, long chapterId) {
    Intent intent = ChapterDetailsActivity.createNewIntent(activity, chapterId);
    activity.startActivity(intent);
  }

  public void openNovel(Activity activity, Novel novel) {
    Intent intent = NovelDetailsActivity.createIntent(activity, novel.id);
    activity.startActivity(intent);
  }

  public void openLastRead(Activity activity) {
    Intent intent = new Intent(activity, LastReadActivity.class);
    activity.startActivity(intent);
  }
}
