package com.rilixtech.totallytranslations;

import android.app.Activity;
import android.os.Build;

public class Utils {

  public static void closeApp(Activity activity) {
    if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 21) {
      activity.finishAffinity();
    } else if (Build.VERSION.SDK_INT >= 21) {
      activity.finishAndRemoveTask();
    } else {
      activity.finish();
    }
    System.exit(0);
  }
}
