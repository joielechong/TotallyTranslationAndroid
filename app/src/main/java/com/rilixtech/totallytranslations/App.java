package com.rilixtech.totallytranslations;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import com.evernote.android.job.JobManager;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.pixplicity.easyprefs.library.Prefs;
import com.rilixtech.totallytranslations.update.UpdateJobCreator;

public class App extends Application {

  private static App mApp;

  public static App getInstance() {
    return mApp;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    AndroidThreeTen.init(this);

    mApp = this;
    ObjectBox.init(this);
    initializePref(this);

    JobManager.create(this).addJobCreator(new UpdateJobCreator());
  }

  private void initializePref(Context context) {
    new Prefs.Builder()
        .setContext(context)
        .setMode(ContextWrapper.MODE_PRIVATE)
        .setPrefsName(getPackageName())
        .setUseDefaultSharedPreference(true)
        .build();
  }
}
