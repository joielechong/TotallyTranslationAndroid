package com.rilixtech.totallytranslations.interactor;

import com.rilixtech.totallytranslations.ObjectBox;

public class DbUtils {

  public static void resetDatabase() {
    ObjectBox.get().close();
    ObjectBox.get().deleteAllFiles();
  }
}
