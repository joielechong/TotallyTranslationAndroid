package com.rilixtech.totallytranslations.interactor;

import com.rilixtech.totallytranslations.ObjectBox;
import com.rilixtech.totallytranslations.model.History;
import io.objectbox.Box;
import java.util.List;
import org.threeten.bp.Instant;

public class HistoryUtils {

  private static History createFirstTimeHistory(long createdTime) {
    Box<History> historyBox = ObjectBox.get().boxFor(History.class);
    History history = new History();
    history.createdTime = createdTime;
    history.lastUpdateTime = createdTime;
    history.id = historyBox.put(history);
    return history;
  }

  public static long updateHistoryInDB(long updatedTime) {
    Box<History> historyBox = ObjectBox.get().boxFor(History.class);
    List<History> histories = historyBox.getAll();

    long id;
    if (histories.size() == 0) {
      id = createFirstTimeHistory(updatedTime).id;
    } else {
      History history = histories.get(0);
      history.lastUpdateTime = updatedTime;
      id = history.id;
    }

    return id;
  }

  public static long getLastHistoryUpdateTime() {
    Box<History> historyBox = ObjectBox.get().boxFor(History.class);
    List<History> histories = historyBox.getAll();

    long lastUpdateTime;
    if (histories.size() == 0) {
      Instant instant = Instant.now();
      lastUpdateTime = createFirstTimeHistory(instant.toEpochMilli()).lastUpdateTime;
    } else {
      History history = histories.get(0);
      lastUpdateTime = history.lastUpdateTime;
    }

    return lastUpdateTime;
  }
}
