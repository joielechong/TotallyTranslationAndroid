package com.rilixtech.totallytranslations.interactor;

import android.util.Log;
import com.rilixtech.totallytranslations.ObjectBox;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Novel;
import com.rilixtech.totallytranslations.model.Novel_;
import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class NovelUtils {
  public static final String TAG = NovelUtils.class.getSimpleName();

  private static List<Chapter> mergeChapters(List<Chapter> currentChapters,
      List<Chapter> newChapters) {
    if (newChapters == null || newChapters.size() == 0) return currentChapters;

    for (Chapter currChapter : currentChapters) {
      for (Iterator<Chapter> iterator = newChapters.iterator(); iterator.hasNext(); ) {
        Chapter newChapter = iterator.next();
        if (currChapter.title.equals(newChapter.title)) {
          currChapter.updateWith(newChapter);
          iterator.remove();
        }
      }
      if (newChapters.size() == 0) {
        break;
      }
    }
    // add the remaining chapters to novel
    if (newChapters.size() > 0) currentChapters.addAll(newChapters);

    return currentChapters;
  }

  public static long updateNovelInDB(Novel newNovel) {
    Novel novel = findNovelByName(newNovel.name);
    if (novel == null) {
      novel = newNovel;
    } else {
      mergeChapters(novel.chapters, newNovel.chapters);
      novel.updateWithoutChapterWith(newNovel);
    }

    Box<Novel> novelBox = ObjectBox.get().boxFor(Novel.class);
    return novelBox.put(novel);
  }

  public static void updateNovel(Novel newNovel) {
    Novel novel = findNovelByName(newNovel.name);
    if (novel == null) {
      novel = newNovel;
    } else {
      //mergeChapters(novel.chapters, newNovel.chapters);
      novel.updateWith(newNovel);
    }
    //newNovel = novel;
    Box<Novel> novelBox = ObjectBox.get().boxFor(Novel.class);
    Log.d(TAG, "novel = " + novel.toString());
    Log.d(TAG, "chapter size = " + novel.chapters.size());
    novelBox.put(novel);
  }

  public static void createNovelsInDB(List<Novel> novels) {
    Box<Novel> novelBox = ObjectBox.get().boxFor(Novel.class);
    novelBox.put(novels);
  }

  public static long findNovelIdByName(String name) {
    Novel novel = findNovelByName(name);
    if (novel == null) return -1;
    return novel.id;
  }

  public static Novel findNovelById(long id) {
    return ObjectBox.get().boxFor(Novel.class).get(id);
  }

  private static Novel findNovelByName(String name) {
    Box<Novel> novelBox = ObjectBox.get().boxFor(Novel.class);
    QueryBuilder<Novel> builder = novelBox.query();
    builder.equal(Novel_.name, name);
    return builder.build().findFirst();
  }

  public static List<Novel> getAllNovels() {
    return ObjectBox.get().boxFor(Novel.class).getAll();
  }

  public static List<Novel> getAllNovelsWithoutChapters() {

    Box<Novel> novelBox = ObjectBox.get().boxFor(Novel.class);
    return novelBox.getAll();
  }

  public static List<Chapter> getNewChaptersFrom(Novel updatedNovel) {
    if (updatedNovel.chapters.size() == 0) return updatedNovel.chapters;

    // get previous novel with the same name
    Novel novel = findNovelByName(updatedNovel.name);
    if (novel == null) return Collections.emptyList();

    // Compare previous novel chapters with updated Novel chapters
    List<Chapter> prevChapters = ChapterUtils.getChapterByNovelId(novel.id);
    if (prevChapters.isEmpty()) return updatedNovel.chapters;

    List<Chapter> updatedChapters = updatedNovel.chapters;
    List<Chapter> newChapters = new ArrayList<>();

    for (Chapter updatedChapter: updatedChapters) {
      boolean isNewChapter = true;
      for (Chapter prevChapter : prevChapters) {
        if (updatedChapter.title.equals(prevChapter.title)) {
          isNewChapter = false;
          break;
        }
      }
      if(isNewChapter) newChapters.add(updatedChapter);
    }
    return newChapters;
  }
}
