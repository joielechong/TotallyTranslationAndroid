package com.rilixtech.totallytranslations.interactor;

import com.rilixtech.totallytranslations.ObjectBox;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Chapter_;
import com.rilixtech.totallytranslations.model.LastReadChapter;
import com.rilixtech.totallytranslations.model.LastReadChapter_;
import com.rilixtech.totallytranslations.model.RecentChapter;
import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.Instant;

public class ChapterUtils {

  /**
   * Update chapter in database
   *
   * @param chapter new found chapter
   * @return id of the chapter.
   */
  public static long updateChapterInDB(Chapter chapter) {
    Box<Chapter> chapterBox = ObjectBox.get().boxFor(Chapter.class);
    QueryBuilder<Chapter> builder = chapterBox.query();
    builder.equal(Chapter_.title, chapter.title)
        .or().equal(Chapter_.recentTitle, chapter.recentTitle);
    Chapter prevChapter = builder.build().findFirst();

    if (prevChapter == null) {
      prevChapter = chapter;
    } else {
      prevChapter.updateWith(chapter);
    }
    return chapterBox.put(prevChapter);
  }

  public static Chapter getChapterById(long chapterId) {
    return ObjectBox.get().boxFor(Chapter.class).get(chapterId);
  }

  public static Chapter getChapterByUrl(String url) {
    Box<Chapter> chapterBox = ObjectBox.get().boxFor(Chapter.class);
    QueryBuilder<Chapter> builder = chapterBox.query();
    builder.equal(Chapter_.url, url);
    return builder.build().findFirst();
  }

  public static Chapter getDetailRecentChapterFrom(Chapter recentChapter) {
    Box<Chapter> chapterBox = ObjectBox.get().boxFor(Chapter.class);
    QueryBuilder<Chapter> builder = chapterBox.query();
    builder.equal(Chapter_.title, recentChapter.title)
        .or()
        .equal(Chapter_.recentTitle, recentChapter.recentTitle);

    return builder.build().findFirst();
  }

  public static List<Chapter> getChapterByNovelId(long novelId) {
    Box<Chapter> chapterBox = ObjectBox.get().boxFor(Chapter.class);
    QueryBuilder<Chapter> builder = chapterBox.query();
    builder.equal(Chapter_.novelId, novelId);
    return builder.build().find();
  }

  public static void saveRecentChapters(List<Chapter> chapters) {
    Box<RecentChapter> box = ObjectBox.get().boxFor(RecentChapter.class);
    List<RecentChapter> recentChapters = new ArrayList<>();
    for (Chapter chapter : chapters) {
      RecentChapter recentChapter = new RecentChapter();
      recentChapter.chapter.setTarget(chapter);
      recentChapters.add(recentChapter);
    }
    box.put(recentChapters);
  }

  public static void clearRecentChapters() {
    Box<RecentChapter> box = ObjectBox.get().boxFor(RecentChapter.class);
    box.removeAll();
  }

  public static List<RecentChapter> getRecentChapters() {
    Box<RecentChapter> recentChapterBox = ObjectBox.get().boxFor(RecentChapter.class);
    return recentChapterBox.getAll();
  }

  //public static boolean isAnyRecentChaptersInDb() {
  //  Box<RecentChapter> recentChapterBox = ObjectBox.get().boxFor(RecentChapter.class);
  //  return recentChapterBox.getAll().size() > 0;
  //}

  public static List<Chapter> getUnreadChapterAfter(long selectedTime) {
    Box<Chapter> chapterBox = ObjectBox.get().boxFor(Chapter.class);
    QueryBuilder<Chapter> builder = chapterBox.query();
    builder.equal(Chapter_.lastReadTime, 0)
        .and().equal(Chapter_.updatedTime, selectedTime)
        .or().greater(Chapter_.updatedTime, selectedTime);

    return builder.build().find();
  }

  public static void saveChapterAsLastRead(long chapterId) {
    Chapter chapter = getChapterById(chapterId);
    if(chapter == null) return;

    LastReadChapter lastReadChapter = getLastReadChapterByChapterId(chapterId);

    Box<LastReadChapter> box = ObjectBox.get().boxFor(LastReadChapter.class);

    Instant instant = Instant.now();
    long time = instant.toEpochMilli();

    if(lastReadChapter == null) {
      lastReadChapter = new LastReadChapter();
      lastReadChapter.chapterId = chapterId;
      lastReadChapter.lastReadTime = time;
      box.put(lastReadChapter);
    } else {
      lastReadChapter.lastReadTime = time;
      box.put(lastReadChapter);
    }
  }

  public static LastReadChapter getLastReadChapterByChapterId(long chapterId) {
    Box<LastReadChapter> box = ObjectBox.get().boxFor(LastReadChapter.class);
    QueryBuilder<LastReadChapter> builder = box.query();
    builder.equal(LastReadChapter_.chapterId, chapterId);
    return builder.build().findFirst();
  }

  public static List<LastReadChapter> getLastReadChapters() {
    Box<LastReadChapter> chapterBox = ObjectBox.get().boxFor(LastReadChapter.class);
    QueryBuilder<LastReadChapter> builder = chapterBox.query();
    builder.orderDesc(LastReadChapter_.lastReadTime);

    return builder.build().find(0, 20);
  }
}
