package com.rilixtech.totallytranslations.model;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Index;
import io.objectbox.annotation.Unique;
import io.objectbox.relation.ToOne;
import java.io.Serializable;

@Entity
public class Chapter implements Serializable {
  @Id public long id;
  @Index public String title;
  @Index @Unique public String recentTitle;
  public String content;
  public String url;
  public String poster;
  public String releaseDate;
  public String novelTitle;
  public String novelUrl;

  public long novelId; // ToOne target ID property
  public ToOne<Novel> novel;

  public long prevChapterId;
  public long nextChapterId;

  public long createdTime; // Time when chapter is added to database. This must not set outside the model
  public long updatedTime;

  public long lastReadTime;

  public Chapter() {
    title = "";
    recentTitle = "";
    content = "";
    url = "";
    poster = "";
    releaseDate = "";
    novelTitle = "";
    novelUrl = "";
    lastReadTime = 0;
    createdTime = 0;
    updatedTime = 0;
  }

  public void updateWith(Chapter chapter) {
    if(!chapter.recentTitle.isEmpty()) recentTitle = chapter.recentTitle;
    if(!chapter.content.isEmpty()) content = chapter.content;
    if(!chapter.url.isEmpty()) url = chapter.url;
    if(!chapter.poster.isEmpty()) poster = chapter.poster;
    if(!chapter.releaseDate.isEmpty()) releaseDate = chapter.releaseDate;
    if(!chapter.novelTitle.isEmpty()) novelTitle = chapter.novelTitle;
    if(!chapter.novelUrl.isEmpty()) novelUrl = chapter.novelUrl;
    if(chapter.updatedTime > 0) updatedTime = chapter.updatedTime;
    if(chapter.createdTime > 0) createdTime = chapter.createdTime;
    if(chapter.lastReadTime > 0) lastReadTime = chapter.lastReadTime;
  }
}
