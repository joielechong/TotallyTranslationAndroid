package com.rilixtech.totallytranslations.model;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

@Entity
public class LastReadChapter {
  @Id public long id;

  public long chapterId; // ToOne target ID property
  public ToOne<Chapter> chapter;
  public long lastReadTime;
}
