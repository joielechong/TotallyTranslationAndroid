package com.rilixtech.totallytranslations.model;

import android.text.TextUtils;
import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Index;
import io.objectbox.annotation.Unique;
import io.objectbox.relation.ToMany;
import java.io.Serializable;

@Entity
public class Novel implements Serializable {
  @Id public long id;
  @Index @Unique public String name;
  public String author;
  public String imageUrl;
  public String url;
  public String description;
  public String synopsis;
  public String patreonUrl;

  @Backlink(to = "novel")
  public ToMany<Chapter> chapters;

  public Novel() {
    name = "";
    author = "";
    imageUrl = "";
    url = "";
    description = "";
    synopsis = "";
    patreonUrl = "";
    //chapters = new ToMany<>(this, Novel_.chapters);
  }

  public void updateWith(Novel novel) {
    updateWithoutChapterWith(novel);
    chapters.addAll(novel.chapters);
  }

  public void updateWithoutChapterWith(Novel novel) {
    if(!novel.name.isEmpty()) name = novel.name;
    if(!novel.author.isEmpty()) author = novel.author;
    if(!novel.imageUrl.isEmpty()) imageUrl = novel.imageUrl;
    if(!novel.url.isEmpty()) url = novel.url;
    if(!novel.description.isEmpty()) description = novel.description;
    if(!novel.synopsis.isEmpty()) synopsis = novel.synopsis;
    if(TextUtils.isEmpty(patreonUrl)) {
      patreonUrl = novel.patreonUrl;
    } else {
      if(!TextUtils.isEmpty(novel.patreonUrl)) patreonUrl = novel.patreonUrl;
    }
  }
}
