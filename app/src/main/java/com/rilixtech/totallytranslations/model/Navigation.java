package com.rilixtech.totallytranslations.model;

public class Navigation {
  private Chapter chapter;
  private String previousChapterId;
  private String previousUrl;
  private String nextChapterId;
  private String nextUrl;

  public Navigation() {
    this.chapter = null;
    this.previousChapterId = "";
    this.previousUrl = "";
    this.nextChapterId = "";
    this.nextUrl = "";
  }

  public Chapter getChapter() {
    return chapter;
  }

  public void setChapter(Chapter chapter) {
    this.chapter = chapter;
  }

  public String getPreviousUrl() {
    return previousUrl;
  }

  public void setPreviousUrl(String previousUrl) {
    this.previousUrl = previousUrl;
  }

  public String getNextUrl() {
    return nextUrl;
  }

  public void setNextUrl(String nextUrl) {
    this.nextUrl = nextUrl;
  }

  public String getPreviousChapterId() {
    return previousChapterId;
  }

  public void setPreviousChapterId(String previousChapterId) {
    this.previousChapterId = previousChapterId;
  }

  public String getNextChapterId() {
    return nextChapterId;
  }

  public void setNextChapterId(String nextChapterId) {
    this.nextChapterId = nextChapterId;
  }
}
