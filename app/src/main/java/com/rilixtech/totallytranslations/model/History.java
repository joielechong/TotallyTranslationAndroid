package com.rilixtech.totallytranslations.model;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class History {

  @Id public long id;
  public long lastUpdateTime;
  public long createdTime;

  public History() {
    lastUpdateTime = 0;
    createdTime = 0;
  }
}
