package com.rilixtech.totallytranslations.chapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.rilixtech.materialfancybutton.MaterialFancyButton;
import com.rilixtech.totallytranslations.BaseActivity;
import com.rilixtech.totallytranslations.R;
import com.rilixtech.totallytranslations.lastread.RefreshLastReadEvent;
import com.rilixtech.totallytranslations.interactor.ChapterUtils;
import com.rilixtech.totallytranslations.manager.ChapterManager;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Navigation;
import com.rilixtech.totallytranslations.setting.PrefUtil;
import org.greenrobot.eventbus.EventBus;

public class ChapterDetailsActivity extends BaseActivity {
  private static final String EXTRA_CHAPTER_ID = "chapterIdExtra";
  @BindView(R.id.chapter_details_content_tv) TextView mTvContent;
  @BindView(R.id.chapter_details_prev_mfb) MaterialFancyButton mfbPrevious;
  @BindView(R.id.chapter_details_next_mfb) MaterialFancyButton mfbNext;
  @BindView(R.id.chapter_details_scrl) ScrollView mScrlContent;
  @BindView(R.id.chapter_details_progress_tv) TextView mTvProgress;
  @BindView(R.id.chapter_details_progress_pgb) ProgressBar mPgbProgress;
  @BindView(R.id.chapter_details_update_pgb) ProgressBar mPgbUpdateChapter;

  private Navigation mNavigation;
  private Chapter mChapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_chapter_details);
    ButterKnife.bind(this);
    setBackAsHome();

    Bundle bundle = getIntent().getExtras();
    if(bundle == null) throw new RuntimeException("Need to add extra chapter id for bundle!!!");
    long id = bundle.getLong(EXTRA_CHAPTER_ID);

    mChapter = ChapterUtils.getChapterById(id);
    getChapterContent(mChapter);

    ChapterUtils.saveChapterAsLastRead(id);
  }

  public static Intent createNewIntent(Activity activity, long chapterId) {
    Intent intent = new Intent(activity, ChapterDetailsActivity.class);
    intent.putExtra(EXTRA_CHAPTER_ID, chapterId);
    return intent;
  }

  private void showProgress(boolean show) {
    mPgbProgress.setVisibility(show ? View.VISIBLE : View.GONE);
    mTvProgress.setVisibility(show ? View.VISIBLE : View.GONE);
  }

  /**
   * Get chapter content of specific chapter
   */
  private void getChapterContent(Chapter chapter) {
    setActivityTitle(chapter.title);
    if (TextUtils.isEmpty(chapter.content)) {
      showProgress(true);
    } else {
      setContentToTextView(chapter.content);
      showProgress(false);
      mPgbUpdateChapter.setVisibility(View.VISIBLE);
    }

    // update content when empty or when Always update chapter setting is enabled.
    if(TextUtils.isEmpty(chapter.content) || PrefUtil.isUpdateChapterWhenOpening(this)) {
      getChapterContentFromSite(chapter);
    } else {
      showProgress(false);
      mPgbUpdateChapter.setVisibility(View.GONE);
    }
  }

  private void getChapterContentFromSite(Chapter chapter) {
    ChapterDetailsTask task = new ChapterDetailsTask(chapter,
        new ChapterDetailsTask.ResultListener() {
          @Override public void onSuccess(Chapter updatedChapter, Navigation navigation) {
            mNavigation = navigation;
            setButtonStateBy(mNavigation);
            setContentToTextView(updatedChapter.content);
            setActivityTitle(updatedChapter.title);
            ChapterManager.saveChapter(chapter);
            ChapterUtils.saveChapterAsLastRead(chapter.id);
            showProgress(false);
            mPgbUpdateChapter.setVisibility(View.GONE);
            EventBus.getDefault().post(new RefreshLastReadEvent());
          }

          @Override public void onFailure(String error) {
            showProgress(false);
            mPgbUpdateChapter.setVisibility(View.GONE);
          }
        });
    task.execute();
  }

  private void setContentToTextView(String content) {
    mScrlContent.smoothScrollTo(0, 0); // scroll to top of screen
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      mTvContent.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT));
    } else {
      mTvContent.setText(Html.fromHtml(content));
    }
  }

  private void setButtonStateBy(Navigation navigation) {
    if (navigation.getPreviousUrl().isEmpty()) {
      mfbPrevious.setVisibility(View.GONE);
    } else {
      mfbPrevious.setVisibility(View.VISIBLE);
    }

    if (navigation.getNextUrl().isEmpty()) {
      mfbNext.setVisibility(View.GONE);
    } else {
      mfbNext.setVisibility(View.VISIBLE);
    }
  }

  @OnClick(R.id.chapter_details_prev_mfb)
  protected void onPreviousButtonClicked() {
    loadSpecificChapter(mChapter.prevChapterId, mNavigation.getPreviousUrl());
  }

  @OnClick(R.id.chapter_details_next_mfb)
  protected void onNextButtonClicked() {
    loadSpecificChapter(mChapter.nextChapterId, mNavigation.getNextUrl());
  }

  private void loadSpecificChapter(long chapterId, String url) {
    if (chapterId > 0) {
      Chapter chapter = ChapterUtils.getChapterById(chapterId);
      if (chapter == null) {
        chapter = ChapterUtils.getChapterByUrl(url);
        if (chapter == null) {
          getChapterDetails(url);
        } else {
          mChapter = chapter;
          getChapterContent(mChapter);
        ChapterUtils.saveChapterAsLastRead(chapter.id);
        }
      } else {
        mChapter = chapter;
        getChapterContent(mChapter);
        ChapterUtils.saveChapterAsLastRead(chapter.id);
      }
      EventBus.getDefault().post(new RefreshLastReadEvent());
    } else {
      Chapter chapter = ChapterUtils.getChapterByUrl(url);
      if (chapter == null) {
        getChapterDetails(url);
      } else {
        mChapter = chapter;
        getChapterContent(mChapter);
      }
    }
  }

  private void getChapterDetails(String url) {
    Chapter chapter = new Chapter();
    chapter.url = url;
    getChapterContent(chapter);
  }
}
