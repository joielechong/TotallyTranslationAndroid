package com.rilixtech.totallytranslations.chapter;

import android.os.AsyncTask;
import android.util.Pair;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Navigation;
import com.rilixtech.totallytranslations.parser.ChapterParser;

public class ChapterDetailsTask extends AsyncTask<Void, Void, Pair<Chapter, Navigation>> {
  public interface ResultListener {
    void onSuccess(Chapter updatedChapter, Navigation navigation);
    void onFailure(String error);
  }

  private ResultListener mListener;
  private Chapter mChapter;

  ChapterDetailsTask(Chapter chapter, ResultListener listener) {
    mChapter = chapter;
    mListener = listener;
  }

  @Override protected Pair<Chapter, Navigation> doInBackground(Void... voids) {
    return ChapterParser.getChapterDetails(mChapter);
  }

  @Override protected void onPostExecute(Pair<Chapter, Navigation> result) {
    if(result == null) {
      mListener.onFailure("Failed to get details");
    } else {
      mListener.onSuccess(result.first, result.second);
    }
  }
}
