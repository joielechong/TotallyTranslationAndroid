package com.rilixtech.totallytranslations;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutActivity extends BaseActivity {
  @BindView(R.id.about_app_tv) TextView mTvApp;
  @BindView(R.id.about_developer_tv) TextView mTvDeveloper;
  @BindView(R.id.about_app_version) TextView mTvVersion;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_about);
    ButterKnife.bind(this);
    setBackAsHome();
    mTvApp.setMovementMethod(LinkMovementMethod.getInstance());
    Linkify.addLinks(mTvApp, Linkify.WEB_URLS);
    mTvDeveloper.setMovementMethod(LinkMovementMethod.getInstance());
    Linkify.addLinks(mTvDeveloper, Linkify.WEB_URLS);
    String buildVersion = BuildConfig.VERSION_NAME;
    mTvVersion.setText(getString(R.string.about_version, buildVersion));
  }
}
