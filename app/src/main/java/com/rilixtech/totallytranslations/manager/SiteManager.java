package com.rilixtech.totallytranslations.manager;

import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;
import com.rilixtech.totallytranslations.interactor.ChapterUtils;
import com.rilixtech.totallytranslations.interactor.NovelUtils;
import com.rilixtech.totallytranslations.model.Chapter;
import com.rilixtech.totallytranslations.model.Novel;
import com.rilixtech.totallytranslations.model.RecentChapter;
import com.rilixtech.totallytranslations.parser.NovelParser;
import com.rilixtech.totallytranslations.parser.SiteParser;
import java.util.List;
import org.threeten.bp.Instant;

public class SiteManager {

  public static final String TAG = "SiteManager";

  public static void createOrUpdateNovels(List<Novel> novels) {
    // check if we don't have any novels yet
    List<Novel> oldNovels = NovelUtils.getAllNovels();
    if (oldNovels.size() == 0) {
      // set all created time for chapter as current time
      Instant instant = Instant.now();
      long currentTime = instant.toEpochMilli();
      for(Novel novel : novels) {
        List<Chapter> chapters = novel.chapters;
        if(chapters != null && chapters.size() > 0) {
          for(Chapter chapter: chapters) {
            if(chapter.createdTime == 0) {
              chapter.createdTime = currentTime;
              chapter.updatedTime = currentTime;
            }
          }
        }
      }

      NovelUtils.createNovelsInDB(novels);
    }
  }

  public static List<RecentChapter> saveAndGetRecentChaptersToDB(Context ctx,
      List<Chapter> recentChapters) {
    for (int i = 0; i < recentChapters.size(); i++) {
      Chapter recentChapter = recentChapters.get(i);

      // Check if there is no previous saved chapter
      Chapter oldChapter = ChapterUtils.getDetailRecentChapterFrom(recentChapter);
      if (oldChapter == null) {
        long novelId = NovelUtils.findNovelIdByName(recentChapter.novelTitle);
        if (novelId <= 0) {
          Toast.makeText(ctx, "No related novel found", Toast.LENGTH_SHORT).show();
          break;
        }
        recentChapter.novelId = novelId;
        recentChapter.id = ChapterManager.saveChapter(recentChapter);
      } else {
        oldChapter.updateWith(recentChapter);
        recentChapters.set(i, oldChapter);
      }
    }

    // delete previous recent chapters
    ChapterUtils.clearRecentChapters();
    ChapterUtils.saveRecentChapters(recentChapters);

    return ChapterUtils.getRecentChapters();
  }

  public static void getSiteDetailsSynchronous(Context ctx) {
    Pair<List<Novel>, List<Chapter>> novelAndRecentsChapters = SiteParser.getSiteNovels();

    if(novelAndRecentsChapters == null) {
      Log.e(TAG, "Failed to download novel and recent chapters");
      return;
    }
    createOrUpdateNovels(novelAndRecentsChapters.first);
    SiteManager.saveAndGetRecentChaptersToDB(ctx, novelAndRecentsChapters.second);

    List<Novel> novels = novelAndRecentsChapters.first;
    for(int i = 0; i < novels.size(); i++) {
      Novel curNovel = novels.get(i);
      Novel novel = NovelParser.getNovelDetails(curNovel.url, true);

      if(novel != null) curNovel.id = novel.id;
    }

  }
}
