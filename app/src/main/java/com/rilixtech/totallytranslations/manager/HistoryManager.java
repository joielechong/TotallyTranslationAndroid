package com.rilixtech.totallytranslations.manager;

import com.rilixtech.totallytranslations.interactor.ChapterUtils;
import com.rilixtech.totallytranslations.interactor.HistoryUtils;
import com.rilixtech.totallytranslations.model.Chapter;
import java.util.List;

public class HistoryManager {

  public static void updateHistoryLastUpdate(long lastUpdateTime) {
    HistoryUtils.updateHistoryInDB(lastUpdateTime);
  }

  public static List<Chapter> getLastUnreadChapters() {
    long lastUpdateTime = HistoryUtils.getLastHistoryUpdateTime();
    return ChapterUtils.getUnreadChapterAfter(lastUpdateTime);
  }
}
