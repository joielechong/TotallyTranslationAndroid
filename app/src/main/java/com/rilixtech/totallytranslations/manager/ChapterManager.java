package com.rilixtech.totallytranslations.manager;

import com.rilixtech.totallytranslations.interactor.ChapterUtils;
import com.rilixtech.totallytranslations.model.Chapter;
import org.threeten.bp.Instant;

public class ChapterManager {
  public static long saveChapter(Chapter chapter){
    Instant instant = Instant.now();
    chapter.updatedTime = instant.toEpochMilli();
    if(chapter.createdTime == 0) {
      chapter.createdTime = chapter.updatedTime;
    }
    return ChapterUtils.updateChapterInDB(chapter);
  }

}
