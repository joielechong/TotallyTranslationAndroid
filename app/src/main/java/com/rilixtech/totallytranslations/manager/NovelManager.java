package com.rilixtech.totallytranslations.manager;

import com.rilixtech.totallytranslations.interactor.NovelUtils;
import com.rilixtech.totallytranslations.model.Novel;

public class NovelManager {
  public static Novel updateNovel(Novel currentNovel, Novel newNovel) {
    currentNovel.updateWith(newNovel);
    currentNovel.id = NovelUtils.updateNovelInDB(currentNovel);
    return currentNovel;
  }

  public static Novel getNovelById(long novelId) {
    return NovelUtils.findNovelById(novelId);
  }
}
